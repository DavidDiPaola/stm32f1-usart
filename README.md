# usart
stm32f1 universal synchronous/asynchronous receiver/transmitter library

[GitLab](https://gitlab.com/DavidDiPaola/stm32f1-usart)
[GitHub](https://github.com/DavidDiPaola/stm32f1-usart)

to be used with [stm32f1-common](https://gitlab.com/DavidDiPaola/stm32f1-common)

